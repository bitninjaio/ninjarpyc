import uuid

#TODO: getters, setters for debug-related properties

class RemoteCommand:
    
    # Instance variables are
    # __serviceName
    # __methodName
    # __params
    # __responseQueueName
    # __corelationId
    # __result
    # __traceId
    # __spanId
    # __parentId
    # __sampled
    # __flags

    def __init__(self) :
        return
    
    @classmethod
    def create(cls, serviceName, methodName, params, responseQueueName) :
        instance = RemoteCommand()
        instance.__corelationId = str(uuid.uuid4());    # Will this work with PHP implementations?
        instance.__serviceName = serviceName
        instance.__methodName = methodName
        instance.__params = params
        instance.__responseQueueName = responseQueueName
        instance.__result = None
        instance.__traceId = None
        instance.__spanId = None
        instance.__parentId = None
        instance.__sampled = None
        instance.__flags = None
        return instance
    
    def toDict(self) :
        return {
                    'corelationId': self.__corelationId,
                    'serviceName': self.__serviceName,
                    'methodName': self.__params,
                    'responseQueueName': self.__responseQueueName,
                    'result': self.__result,
                    'traceId': self.__traceId,
                    'spanId': self.__spanId,
                    'parentId': self.__parentId,
                    'sampled': self.__sampled,
                    'flags': self.__flags,
                }

    @classmethod
    def createFromDict(cls, dct) :
        instance = RemoteCommand();
        instance.__corelationId = dct['corelationId'];
        instance.__serviceName = dct['serviceName'];
        instance.__methodName = dct['methodName'];
        instance.__responseQueueName = dct['responseQueueName'];
        instance.__result = dct['result'];
        instance.__traceId = dct['traceId'];
        instance.__spanId = dct['spanId'];
        instance.__parentId = dct['parentId'];
        instance.__sampled = dct['sampled'];
        instance.__flags = dct['flags'];
        return instance
    
    def getCorrelationId(self) :
        return self.__corelationId
    
    def getMethodName(self) :
        return self.__methodName
    
    def getParams(self) :
        return self.__params
    
    def getResponseQueueName(self) :
        return self.__responseQueueName
    
    def getResult(self) :
        return self.__result
    
    def getFlags(self) :
        return self.__flags

    def setResult(self, result) :
        self.__result = result

    def setFlags(self, flags) :
        self.__flags = flags